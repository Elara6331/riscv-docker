# busybox-root

The [busybox-root image](https://gitea.elara.ws/Elara6331/-/packages/container/busybox-root/latest) is a basic image with the busybox utilities installed.

This image runs as root. See [busybox](https://gitea.elara.ws/Elara6331/riscv-docker/src/branch/master/busybox) for a rootless container.
